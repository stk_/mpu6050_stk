
/* MPU6050 Basic Example Code
 by: Stefan Kiegerl
 date: October 14, 2020
 license: Beerware - Use this code however you'd like. If you
 find it useful you can buy me a beer some time.
 
 Most Code is taken from from Kris Winer's MPU6050 library. [https://os.mbed.com/users/onehorse/code/MPU6050IMU/]
 */

#include "MPU6050.h"
#include "mbed.h"
#include <cstdint>

int main() {

  MPU6050 mpu6050(D0, D1); // D1 = I2C1_SCL | D0 = I2C1_SDA

//     Timer timer;

  float acc_data[3];
  float gyro_data[3];
  float angles[3];
  float yaw, pitch, roll;
  float temperature;

    // timer.start();
  // Read the WHO_AM_I register, this is a good test of communication
  uint8_t whoami = mpu6050.readByte(
      MPU6050_ADDRESS, WHO_AM_I_MPU6050); // Read WHO_AM_I register for MPU-6050
  printf("I AM 0x%x\n\r", whoami);
  printf("I SHOULD BE 0x68\n\r");

  if (mpu6050.testConnection()) {
    printf("MPU6050 is online...\n\r");
    wait_us(1000000);

    float selfTest[6];
    mpu6050.SelfTest(
        selfTest); // Start by performing self test and reporting values
    printf("x-axis self test: acceleration trim within : ");
    printf("%f", selfTest[0]);
    printf("% of factory value \n\r");
    printf("y-axis self test: acceleration trim within : ");
    printf("%f", selfTest[1]);
    printf("% of factory value \n\r");
    printf("z-axis self test: acceleration trim within : ");
    printf("%f", selfTest[2]);
    printf("% of factory value \n\r");
    printf("x-axis self test: gyration trim within : ");
    printf("%f", selfTest[3]);
    printf("% of factory value \n\r");
    printf("y-axis self test: gyration trim within : ");
    printf("%f", selfTest[4]);
    printf("% of factory value \n\r");
    printf("z-axis self test: gyration trim within : ");
    printf("%f", selfTest[5]);
    printf("% of factory value \n\r");
    wait_us(1000000);

    if (selfTest[0] < 1.0f && selfTest[1] < 1.0f && selfTest[2] < 1.0f &&
        selfTest[3] < 1.0f && selfTest[4] < 1.0f && selfTest[5] < 1.0f) {
      mpu6050.reset(); // Reset registers to default in preparation for
                              // device calibration

      mpu6050.calibrate(); // Calibrate gyro and accelerometers,
                                  // load biases in bias registers
      mpu6050.init();
      printf(
          "MPU6050 initialized for active data mode....\n\r"); // Initialize
                                                               // device for
                                                               // active mode
                                                               // read of
                                                               // acclerometer,
                                                               // gyroscope, and
                                                               // temperature

      wait_us(2000000);
    } else {
      printf("Device did not the pass self-test!\n\r");
    }
  } else {
    printf("Could not connect to MPU6050: \n\r");
    printf("%#x \n", whoami);

    while (1)
      ; // Loop forever if communication doesn't happen
  }

  mpu6050.setAccelResolution(AFS_8G);
  mpu6050.setGyroResolution(GFS_250DPS);
  while (1) {
    
    // If data ready bit set, all data registers have new data
    if (mpu6050.readByte(MPU6050_ADDRESS, INT_STATUS) &
        0x01) { // check if data ready interrupt

      mpu6050.readAccelGForce(acc_data);
      mpu6050.readGyroDPS(gyro_data);
      // temperature = mpu6050.readTemperature();

    float frequency;

      mpu6050.computeAngle(acc_data, gyro_data, angles, &frequency);
      yaw = angles[0];
      pitch = angles[1];
      roll = angles[2];
    
    //   printf("a0 = %f | ", acc_data[0]);
    //   printf("a1 = %f | ", acc_data[1]);
    //   printf("a2 = %f\n\r", acc_data[2]);

      // printf("g0 = %f | ", gyro_data[0]);
      // printf("g1 = %f | ", gyro_data[1]);
      // printf("g2 = %f\n\r", gyro_data[2]);
    // if(timer.read_ms() >= 1000){
    //   printf("average rate = %f Hz\n\r", frequency);

    //   printf("roll pitch yaw\n\r");
      printf("%f", roll);
      printf("/");
      printf("%f", pitch);
      printf("/");
      printf("%f\r\n", yaw);

    //   timer.reset();
    // }
    }
  }
}
